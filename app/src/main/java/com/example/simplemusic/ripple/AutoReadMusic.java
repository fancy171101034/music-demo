package com.example.simplemusic.ripple;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.widget.Button;
import android.widget.Toast;
import com.acrcloud.rec.*;
import com.acrcloud.rec.utils.ACRCloudLogger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AutoReadMusic  implements IACRCloudListener, IACRCloudRadioMetadataListener {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    private boolean mProcessing = false;
    private boolean mAutoRecognizing = false;
    private boolean initState = false;
    private String path = "";
    private ACRCloudConfig mConfig = null;
    private ACRCloudClient mClient = null;
    private  Context context;
    private  String result;


    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public  AutoReadMusic(Button start, Button stop, Context context){

        this.context =context;
        path = Environment.getExternalStorageDirectory().toString()
                + "/acrcloud";


        File file = new File(path);
        if(!file.exists()){
            file.mkdirs();
        }

        verifyPermissions();

        this.mConfig = new ACRCloudConfig();

        this.mConfig.acrcloudListener = this;
        this.mConfig.context = context;

        // Please create project in "http://console.acrcloud.cn/service/avr".
        this.mConfig.host = "identify-cn-north-1.acrcloud.cn";
        this.mConfig.accessKey = "7eac2bb83dc0b0e127f27e4d87670807";
        this.mConfig.accessSecret = "RrhFhcyaWirwIokxEuYxJAZ5Y2M1mpn3BN9kXTPA";


        // auto recognize access key
        this.mConfig.hostAuto = "";
        this.mConfig.accessKeyAuto = "";
        this.mConfig.accessSecretAuto = "";

        this.mConfig.recorderConfig.rate = 8000;
        this.mConfig.recorderConfig.channels = 1;

        this.mConfig.recorderConfig.isVolumeCallback = true;

        this.mClient = new ACRCloudClient();
        ACRCloudLogger.setLog(true);

        this.initState = this.mClient.initWithConfig(this.mConfig);

    }





    public void start() {
        if (!this.initState) { return;      }
        if (!mProcessing) {  mProcessing = true;
            if (this.mClient == null || !this.mClient.startRecognize()) {   mProcessing = false;   }
        }
    }


    public void cancel() {
        if (mProcessing && this.mClient != null) {  this.mClient.cancel();        }
        this.reset();    }

    public void openAutoRecognize() {
        String str = "开始";
        if (!mAutoRecognizing) {
            mAutoRecognizing = true;
            if (this.mClient == null || !this.mClient.runAutoRecognize()) {
                mAutoRecognizing = true;
                str = "失败";
            }
        }
            }

    public void closeAutoRecognize() {
        String str = "成功";
        if (!mAutoRecognizing) {
            mAutoRecognizing = true;
            if (this.mClient == null || !this.mClient.runAutoRecognize()) {
                mAutoRecognizing = true;
                str = "失败";
            }
        }
           }

    // callback IACRCloudRadioMetadataListener
    public void requestRadioMetadata() {
        String lat = "39.98";
        String lng = "116.29";
        List<String> freq = new ArrayList<>();
        freq.add("88.7");
        if (!this.mClient.requestRadioMetadataAsyn(lat, lng, freq,
                ACRCloudConfig.RadioType.FM, this)) {
            String str = "失败";
       }
    }

    public void reset() {
        mProcessing = false;
    }


    public void onResult(ACRCloudResult results) {
        this.reset();
        String result = results.getResult();
        String tres = "\n";
        try {
            JSONObject j = new JSONObject(result);
            JSONObject j1 = j.getJSONObject("status");
            int j2 = j1.getInt("code");
            if(j2 == 0){
                JSONObject metadata = j.getJSONObject("metadata");
                //
                if (metadata.has("music")) {
                    JSONArray musics = metadata.getJSONArray("music");
                    for(int i=0; i<musics.length(); i++) {
                        JSONObject tt = (JSONObject) musics.get(i);
                        String title = tt.getString("title");
                        JSONArray artistt = tt.getJSONArray("artists");
                        JSONObject art = (JSONObject) artistt.get(0);
                        String artist = art.getString("name");
                        tres = tres + (i+1) + ".  Title: " + title + "    Artist: " + artist + "\n";
                    }
                }

                tres = tres + "\n\n" + result;
            }else{
                tres = result;
            }
        } catch (JSONException e) {
            tres = result;
            e.printStackTrace();
        }
        //
        setResult(tres);
    }




    private static String[] PERMISSIONS = {
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.RECORD_AUDIO
    };

    public void verifyPermissions() {
        for (int i=0; i<PERMISSIONS.length; i++) {
            int permission = ActivityCompat.checkSelfPermission(context, PERMISSIONS[i]);
            if (permission != PackageManager.PERMISSION_GRANTED) {
                break;
            }
        }
    }


    protected void onDestroy() {
        if (this.mClient != null) {
            this.mClient.release();
            this.initState = false;
            this.mClient = null;
        }
    }

    @Override
    public void onRadioMetadataResult(String s) {      }
    @Override
    public void onVolumeChanged(double volume) {     }


    public static void main(String[] args) {
        System.out.println("helloworld");
    }
}
