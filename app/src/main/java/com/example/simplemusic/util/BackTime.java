package com.example.simplemusic.util;


import android.os.CountDownTimer;
import android.widget.TextView;

public class BackTime  extends CountDownTimer {
    public static final int TIME_COUNT = 5000;//倒计时总时间为5S
    private TextView btn;
    private String endStrRid;
    int number =5;


    public BackTime(long millisInFuture, long countDownInterval,TextView btn,String endStrRid) {
        super(millisInFuture, countDownInterval);
        this.btn=btn;
        this.endStrRid=endStrRid;
    }


    public BackTime(TextView btn,String endStrRid) {
        super(TIME_COUNT,1000);
        this.btn=btn;
        this.endStrRid=endStrRid;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        //每隔一秒修改一次UI
        btn.setText(number);
        number--;

    }

    @Override
    public void onFinish() {
        btn.setText(endStrRid);
        btn.setEnabled(true);
    }
}
