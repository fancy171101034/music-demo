package com.example.simplemusic.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.simplemusic.R;
import com.example.simplemusic.adapter.MusicAdapter;
import com.example.simplemusic.bean.Music;
import com.example.simplemusic.db.MyMusic;
import com.example.simplemusic.service.MusicService;
import com.example.simplemusic.util.MusicUtils;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.BIND_AUTO_CREATE;


public class FragmentOne  extends Fragment  {
    private Handler mHandler = new Handler();



    private  ListView listView;
    private static List<Music> musicList;
    private MusicAdapter musicAdapter;
    View view;
    private MusicService.MusicServiceBinder serviceBinder;
    Intent  intent;
    Button refresh1;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("xx  Context",context.toString());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view =inflater.inflate(R.layout.fragmentone,container,false);

        listView = (ListView) view.findViewById(R.id.local_music_listview);
//        refresh1=view.findViewById(R.id.refresh1);

        musicList = new ArrayList<>();
        musicList = MusicUtils.getMusicData(this.getContext());
        bindService();
        // 音乐列表绑定适配器
        musicAdapter = new MusicAdapter(this.getContext(), R.layout.music_item, musicList);
        listView.setAdapter(musicAdapter);




        // 列表项点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Music music = musicList.get(position);
//                Toast.makeText(getContext(),musicList.get(position).getTitle(),Toast.LENGTH_LONG).show();

                Log.e("xx",music.getTitle().toString()+"   "+music.getArtist().toLowerCase());
                serviceBinder.addPlayList(music);
            }
        });


//         列表项中更多按钮的点击事件
        musicAdapter.setOnMoreButtonListener(new MusicAdapter.onMoreButtonListener() {
            @Override
            public void onClick(final int i) {
                final Music music = musicList.get(i);

                //弹出操作对话框
                final String[] items = new String[] {"添加到播放列表", "删除"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(music.title+"-"+music.artist);

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                serviceBinder.addPlayList(music);
                                break;
                            case 1:
                                //从列表和数据库中删除
                                musicList.remove(i);
                                LitePal.deleteAll(MyMusic.class, "title=?", music.title);
                                musicAdapter.notifyDataSetChanged();
                                break;
                        }
                    }
                });
                builder.create().show();
            }
        });
        return view;
    }

    private void bindService() {
        intent = new Intent(getActivity(), MusicService.class);
        getActivity().startService(intent);
        getPermission();

    }

   public void getPermission(){

       if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
           ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
       }else {
           getActivity().bindService(intent,mServiceConnection,BIND_AUTO_CREATE);  //让我痛苦两天的东西解决了
       }






    }

    private ServiceConnection mServiceConnection =new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serviceBinder =(MusicService.MusicServiceBinder) service;
            mHandler.post(mRunnable);
            Log.d("xx", "Service与Fragment已连接");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRunnable, 1000);
        }
    };




}
