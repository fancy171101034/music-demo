package com.example.simplemusic.fragment;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.arialyy.aria.core.Aria;
import com.example.simplemusic.R;
import com.example.simplemusic.activity.MainActivity;
import com.example.simplemusic.activity.OnlineMusicActivity;
import com.example.simplemusic.adapter.MusicAdapter;
import com.example.simplemusic.bean.Music;
import com.example.simplemusic.service.MusicService;
import com.example.simplemusic.util.MusicUtils;
import com.example.simplemusic.util.MyDatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.Context.BIND_AUTO_CREATE;

public class FragmentTwo extends Fragment {


    private Handler mHandler = new Handler();
    View view;
    private ListView listView;
    private static List<Music> musicList;
    private MusicAdapter musicAdapter;
    private OkHttpClient client;
    private Handler mainHanlder;
    private MusicService.MusicServiceBinder serviceBinder;
    Intent intent;
    Aria aria;
    private final String DOWNLOAD_PATH ="Environment.getExternalStorageDirectory().getPath()";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmenttwo, container, false);

        listView = (ListView) view.findViewById(R.id.online_music_listview);
        client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)//设置连接超时时间
                .readTimeout(10, TimeUnit.SECONDS)//设置读取超时时间
                .build();

        musicList = new ArrayList<>();
//        musicList= MusicUtils.getMusicData(getContext());
//        musicList=getMusicList();


        bindService();

        // 音乐列表绑定适配器
        musicAdapter = new MusicAdapter(this.getContext(), R.layout.music_item, musicList);
        listView.setAdapter(musicAdapter);
        getMusicList();

        mainHanlder = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what){
                    case 60:
                        //更新一首歌曲
                        Music music = (Music) msg.obj;
                        musicList.add(music);
                        musicAdapter.notifyDataSetChanged();
                        break;
                }
            }
        };




        // 列表项点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Music music = musicList.get(position);
                serviceBinder.addPlayList(music);//别人写的很不错的直接用了
            }
        });

        //列表项中更多按钮的点击事件
        musicAdapter.setOnMoreButtonListener(new MusicAdapter.onMoreButtonListener() {
            @Override
            public void onClick(final int i) {
                final Music music = musicList.get(i);
                final String[] items = new String[] { "删除","下载"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(music.title+"-"+music.artist);

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                               serviceBinder.removeMusic(i);
                                break;
                            case 1:
                                MyDatabaseHelper helper = new MyDatabaseHelper(getContext(),"DownloadMusic.db",null,1);
                                //检测到没有BookStore这个数据库，会创建该数据库并调用MyDatabaseHelper中的onCreated方法。

                                //检测到没有BookStore这个数据库，会创建该数据库并调用MyDatabaseHelper中的onCreated方法。
                                helper.getWritableDatabase();
                                SQLiteDatabase db = helper.getWritableDatabase();
                                ContentValues values = new ContentValues();

                                Music musicDownload =new Music();
                                musicDownload=musicList.get(i);
                                values.clear();
                                values.put("title",musicDownload.getTitle().toString());
                                values.put("artist",musicDownload.getArtist().toString());
                                values.put("songUrl",musicDownload.getSongUrl().toString());
                                values.put("imgUrl",musicDownload.getImgUrl().toString());
                                db.insert("music",null,values);
                                String DOWNLOAD_URL=musicDownload.getSongUrl();
                                String SONGNAME=musicDownload.getTitle();

                                Log.e("xx",DOWNLOAD_URL);


                                long taskId = Aria.download(getActivity())
                                        .load(DOWNLOAD_URL)     //读取下载地址
                                        .setFilePath("/sdcard/Music/"+SONGNAME+".mp3") //设置文件保存的完整路径
                                        .create();   //创建并启动下载


                                Log.e("xx","2|"+Environment.getExternalStorageDirectory().getPath());


                                musicAdapter.notifyDataSetChanged();
                                Toast.makeText(getContext(),"下载完成!",Toast.LENGTH_LONG).show();
                                break;




                        }
                    }
                });
                builder.create().show();
            }
        });

//


        return view;
    }

    private void bindService() {
        intent =new Intent(getActivity(),MusicService.class);
        getActivity().startService(intent);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }else {
//            getActivity().startService(intent);
            getActivity().bindService(intent,mServiceConnection,BIND_AUTO_CREATE);  //让我痛苦两天的东西解决了
        }
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS}, 1);
            Toast.makeText(getContext(),"添加权限成功",Toast.LENGTH_LONG).show();
        }

    }


    private  ServiceConnection mServiceConnection =new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serviceBinder =(MusicService.MusicServiceBinder) service;
            mHandler.post(mRunnable);
            Log.d("xx", "Service与Fragmenttwo已连接");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(mRunnable, 1000);
        }
    };



    private  void getMusicList(){
        Request request =new Request.Builder().url("https://api.uomg.com/api/rand.music?sort=热歌榜&format=json").build();

       client =new OkHttpClient();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("xx","okttp error");
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String result =response.body().string();
                try {
                    JSONObject obj =new JSONObject(result);
                    JSONObject song =obj.getJSONObject("data");

                    String songurl = song.getString("url");
                    String name = song.getString("name");
                    String singer = song.getString("artistsname");
                    String pic = song.getString("picurl");

//实例化一首音乐并发送到主线程更新
                    Music music = new Music(songurl, name, singer, pic, true);
                    Message message = mainHanlder.obtainMessage();
                    message.what = 60;
                    message.obj = music;
                    mainHanlder.sendMessage(message);





                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Log.e("xx",result);


            }
        });



//        return  musicList;
    }











}