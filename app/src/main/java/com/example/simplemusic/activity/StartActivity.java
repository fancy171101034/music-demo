package com.example.simplemusic.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.simplemusic.R;
import com.example.simplemusic.util.BackTime;

import java.util.Timer;
import java.util.TimerTask;

public class StartActivity extends AppCompatActivity {

    TextView backtime;
    boolean isgoto =false;
    String number ;
    private int i = 5;
    private Timer timer = null;
    private TimerTask task = null;








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);//必须卸载setcontentview之前，否则无效
        setContentView(R.layout.activity_start);


        initview();//初始化视图

        Handler handler= new Handler();
        // 延迟SPLASH_DISPLAY_LENGHT时间然后跳转到MainActivity
        int SPLASH_DISPLAY_LENGHT = 5000;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isgoto)return;
                Intent intent = new Intent(StartActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGHT);
    }

    private void initview() {
        backtime=findViewById(R.id.backtime);

        backtime.bringToFront();
        // 启动计时器
        startTime();

        backtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopTime();
                isgoto =true;
                Intent intent = new Intent(StartActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                return;

            }
        });


    }


    private Handler mHandler = new Handler(){
        public void handleMessage(Message msg) {
            backtime.setText(msg.arg1+"");
            startTime();
        };
    };

    public void startTime(){
        timer = new Timer();
        task = new TimerTask() {

            @Override
            public void run() {
                i--;
                Message  message = mHandler.obtainMessage();
                message.arg1 = i;
                mHandler.sendMessage(message);
            }
        };
        timer.schedule(task, 1000);
    }

    public void stopTime(){
        timer.cancel();
    }




}
